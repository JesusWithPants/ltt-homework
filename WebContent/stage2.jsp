<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.sql.*"%>
<%@page import="oracle.sql.*" %>
<%@page import="java.util.*" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!DOCTYPE html>
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Pavadzīmes pievienošana</title>
</head>
<body>

<%
boolean missingPVZData = false;
String pvzName=request.getParameter("pvzName");
String pvzDate=request.getParameter("pvzDate");
String pvzShipper=request.getParameter("pvzShipper");
String pvzTotal=request.getParameter("pvzTotal");
Connection con = null;
Statement st = null;
ResultSet rs =null;
if((pvzName !=null && !pvzName.equals("")) && (pvzDate !=null && !pvzDate.equals("")) && (pvzShipper !=null && !pvzShipper.equals("")) && (pvzTotal != null && !pvzTotal.equals(""))){
	try {
		Class.forName("oracle.jdbc.OracleDriver");
		con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "lttuser", "parole123");
		con.setAutoCommit(false);
		st = con.createStatement();
		String query = "INSERT INTO pavadzimes(pvzname, pvzdate, pvzshipper, pvzTotal) VALUES ('"+pvzName+"','"+pvzDate+"','"+pvzShipper+"','"+pvzTotal+"')";
		rs = st.executeQuery(query);
		con.commit();
	} 

	catch(Exception ex){
		ex.printStackTrace();
	}
}
else{
	out.print("<font color='red' size='2'>ERROR: NEPILNĪGI PVZ DATI</font><br><br>");
	missingPVZData = true;
}
%>
<table style="width:30%">
		<tr>
			<td>Pavadzīmes Nr.:</td>
			<td><%= request.getParameter("pvzName") %></td>	
		</tr>
		<tr>
			<td>Datums:</td>
			<td><%= request.getParameter("pvzDate") %></td>
		</tr>
		<tr>
			<td>Nosūtītājs:</td>
			<td><%= request.getParameter("pvzShipper") %></td>
		</tr>
	</table>
	<hr>
	<table style="width:80%;text-align:center">
		<tr>
			<td>Npk</td>
			<td>Preces Nosaukums</td>
			<td>Cena</td>
			<td>Daudzums</td>
			<td>Summa</td>
		</tr>
		<tr>
			<td align="center">1.</td>
			<td><%= request.getParameter("thingName1") %></td>
			<td><%= request.getParameter("thingPrice1") %> €</td>
			<td><%= request.getParameter("thingQuantity1") %> gab.</td>
			<td><%= request.getParameter("thingTotal1") %> €</td>
			<%
			String thingName1=request.getParameter("thingName1");
			String thingQuantity1=request.getParameter("thingQuantity1");
			String thingPrice1=request.getParameter("thingPrice1");
			String thingTotal1=request.getParameter("thingTotal1");
			
			if((thingName1 !=null && !thingName1.equals("")) && (thingPrice1 !=null && !thingPrice1.equals("")) && (thingQuantity1 !=null && !thingQuantity1.equals("")) && (thingTotal1 != null && !thingTotal1.equals("")) && missingPVZData != true){
			try {
				st = con.createStatement();
				String thingList1 = "INSERT INTO preces(pvzid, thingname, thingprice, thingquantity, thingtotal) VALUES (PVZID_SEQ.CURRVAL,'"+thingName1+"','"+thingPrice1+"','"+thingQuantity1+"','"+thingTotal1+"')";
				rs = st.executeQuery(thingList1);
				con.commit();
				
			}
			catch(Exception ex){
			ex.printStackTrace();
			}
			}
			%>
		</tr>
		<tr>
			<td align="center">2.</td>
			<td><%= request.getParameter("thingName2") %></td>
			<td><%= request.getParameter("thingPrice2") %> €</td>
			<td><%= request.getParameter("thingQuantity2") %> gab.</td>
			<td><%= request.getParameter("thingTotal2") %> €</td>
						<%
			String thingName2=request.getParameter("thingName2");
			String thingQuantity2=request.getParameter("thingQuantity2");
			String thingPrice2=request.getParameter("thingPrice2");
			String thingTotal2=request.getParameter("thingTotal2");
			
			if((thingName2 !=null && !thingName2.equals("")) && (thingPrice2 !=null && !thingPrice2.equals("")) && (thingQuantity2 !=null && !thingQuantity2.equals("")) && (thingTotal2 != null && !thingTotal2.equals("")) && missingPVZData != true){
			try {
				st = con.createStatement();
				String thingList2 = "INSERT INTO preces(pvzid, thingname, thingprice, thingquantity, thingtotal) VALUES (PVZID_SEQ.CURRVAL,'"+thingName2+"','"+thingPrice2+"','"+thingQuantity2+"','"+thingTotal2+"')";
				rs = st.executeQuery(thingList2);
				con.commit();
				
			}
			catch(Exception ex){
			ex.printStackTrace();
			}
			}
			%>
		</tr>
		<tr>
			<td align="center">3.</td>
			<td><%= request.getParameter("thingName3") %></td>
			<td><%= request.getParameter("thingPrice3") %> €</td>
			<td><%= request.getParameter("thingQuantity3") %> gab.</td>
			<td><%= request.getParameter("thingTotal3") %> €</td>
						<%
			String thingName3=request.getParameter("thingName3");
			String thingQuantity3=request.getParameter("thingQuantity3");
			String thingPrice3=request.getParameter("thingPrice3");
			String thingTotal3=request.getParameter("thingTotal3");
			
			if((thingName3 !=null && !thingName3.equals("")) && (thingPrice3 !=null && !thingPrice3.equals("")) && (thingQuantity3 !=null && !thingQuantity3.equals("")) && (thingTotal3 != null && !thingTotal3.equals("")) && missingPVZData != true){
			try {
				st = con.createStatement();
				String thingList3 = "INSERT INTO preces(pvzid, thingname, thingprice, thingquantity, thingtotal) VALUES (PVZID_SEQ.CURRVAL,'"+thingName3+"','"+thingPrice3+"','"+thingQuantity3+"','"+thingTotal3+"')";
				rs = st.executeQuery(thingList3);
				con.commit();
				
			}
			catch(Exception ex){
			ex.printStackTrace();
			}
			}
			%>
		</tr>
		<tr>
			<td align="center">4.</td>
			<td><%= request.getParameter("thingName4") %></td>
			<td><%= request.getParameter("thingPrice4") %> €</td>
			<td><%= request.getParameter("thingQuantity4") %> gab.</td>
			<td><%= request.getParameter("thingTotal4") %> €</td>
			<%
			String thingName4=request.getParameter("thingName4");
			String thingQuantity4=request.getParameter("thingQuantity4");
			String thingPrice4=request.getParameter("thingPrice4");
			String thingTotal4=request.getParameter("thingTotal4");
			
			if((thingName4 !=null && !thingName4.equals("")) && (thingPrice4 !=null && !thingPrice4.equals("")) && (thingQuantity4 !=null && !thingQuantity4.equals("")) && (thingTotal4 != null && !thingTotal4.equals("")) && missingPVZData != true){
			try {
				st = con.createStatement();
				String thingList4 = "INSERT INTO preces(pvzid, thingname, thingprice, thingquantity, thingtotal) VALUES (PVZID_SEQ.CURRVAL,'"+thingName4+"','"+thingPrice4+"','"+thingQuantity4+"','"+thingTotal4+"')";
				rs = st.executeQuery(thingList4);
				con.commit();
				
			}
			catch(Exception ex){
			ex.printStackTrace();
			}
			}
			%>
		</tr>
		<tr>
			<td align="center">5.</td>
			<td><%= request.getParameter("thingName5") %></td>
			<td><%= request.getParameter("thingPrice5") %> €</td>
			<td><%= request.getParameter("thingQuantity5") %> gab.</td>
			<td><%= request.getParameter("thingTotal5") %> €</td>
			<%
			String thingName5=request.getParameter("thingName5");
			String thingQuantity5=request.getParameter("thingQuantity5");
			String thingPrice5=request.getParameter("thingPrice5");
			String thingTotal5=request.getParameter("thingTotal5");
			
			if((thingName5 !=null && !thingName5.equals("")) && (thingPrice5 !=null && !thingPrice5.equals("")) && (thingQuantity5 !=null && !thingQuantity5.equals("")) && (thingTotal5 != null && !thingTotal5.equals("")) && missingPVZData != true){
			try {
				st = con.createStatement();
				String thingList5 = "INSERT INTO preces(pvzid, thingname, thingprice, thingquantity, thingtotal) VALUES (PVZID_SEQ.CURRVAL,'"+thingName5+"','"+thingPrice5+"','"+thingQuantity5+"','"+thingTotal5+"')";
				rs = st.executeQuery(thingList5);
				con.commit();
				
			}
			catch(Exception ex){
			ex.printStackTrace();
			}
			}
			%>
		</tr>
	</table>
	<hr>
	<table style="width:30%">
		<tr>
			<td>Kopā:</td>
			<td><%= request.getParameter("pvzTotal") %> €</td>
		</tr>
	</table>
	<br>
</body>
<% con.close(); %>
</html>