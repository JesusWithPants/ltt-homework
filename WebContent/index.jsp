<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Pavadzīmes pievienošana</title>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({
    	dateFormat: "dd-M-yy"
    });
  } );
  </script>
  <%
  %>
</head>
<body>
<form action = "stage2.jsp" method = "POST" accept-charset="UTF-8">
	<table>
		<tr>
			<td>Pavadzīmes Nr.:</td>
			<td><input type="text" name="pvzName"/></td>	
		</tr>
		<tr>
			<td>Datums:</td>
			<td><input type="text" name="pvzDate" id="datepicker"/></td>
		</tr>
		<tr>
			<td>Nosūtītājs:</td>
			<td><input type="text" name="pvzShipper"/></td>
		</tr>
	</table>
	<hr>
	<table style="width:80%;text-align:center" class="pvzContents">
	<thead>
		<tr>
			<td>Npk</td>
			<td>Preces Nosaukums</td>
			<td>Cena</td>
			<td>Daudzums</td>
			<td>Summa</td>
		</tr>
	</thead>
	<tbody>
	<c:forEach var="i" begin="1" end="5">
		<tr>
			<td align="center">${i}.</td>
			<td><input name="thingName${i}" size="30"/></td>
			<td><input name="thingPrice${i}" pattern="[0-9.]+" size="10"/> €</td>
			<td><input name="thingQuantity${i}" pattern="[0-9.]+" size="10"/> gab.</td>
			<td><input name="thingTotal${i}" pattern="[0-9.]+" size="10"/> €</td>
		</tr>
	</c:forEach>
	</tbody>
	</table>
	<hr>
	<table style="width:30%">
		<tr>
			<td>Kopā:</td>
			<td><input name="pvzTotal" pattern="[0-9.]+" size="10"/> €</td>
		</tr>
	</table>
	<hr>
	<br>
	<br>
	<input type="submit" value="Pievienot Pavadzīmi">
</form>
</body>
</html>